import {useState, useContext} from 'react';
import AuthContext from '../../context/auth/AuthContext';
import { useDispatch } from 'react-redux';
import { iniciarSesion } from '../../redux/actions/auth';


const LoginForm = () => {

  const [formValues, setFormValues] = useState({
    email:'',
    password:'',
  });

  const dispatch = useDispatch();

  //const {iniciarSesion} = useContext(AuthContext);

  const handleChange =(e) =>{
    setFormValues({...formValues, [e.target.name]: e.target.value});
  }

  const handleSubmit =(e)=>{
    e.preventDefault();
    const {email, password} = formValues;
    //funcion que ejecuta el iniciar sesion de firebase
    //iniciarSesion(email, password);
    dispatch(iniciarSesion(email, password));
  }

  return (
    <form className='border border-1 p-5' onSubmit={handleSubmit}>
      <h2 className='p-3 text-center'>Hola, Bienvenid@</h2>
      <div className='mb-3'>
        <label htmlFor='email' className='form-label'>
          Correo
        </label>
        <input
          type='email'
          className='form-control'
          name='email'
          aria-describedby='emailHelp'
          onChange={handleChange}
          value={formValues.email}
        />
      </div>
      <div className='mb-3'>
        <label htmlFor='exampleInputPassword1' className='form-label'>
          Contraseña
        </label>
        <input
          type='password'
          className='form-control'
          name='password'
          onChange={handleChange}
          value={formValues.password}
        />
      </div>

      <div className='d-grid gap-2 mx-auto'>
        <button type='submit' className='btn btn-primary'>
          Iniciar Sesión
        </button>
      </div>
    </form>
  );
};

export default LoginForm;
