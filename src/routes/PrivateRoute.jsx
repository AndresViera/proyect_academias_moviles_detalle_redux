import Header from '../components/Header';
import Siderbard from '../components/Siderbard';
import {Navigate} from 'react-router-dom';
import { useContext } from 'react';
//import AuthContext from '../context/auth/AuthContext';
import { useDispatch, useSelector } from 'react-redux';
import { cerrarSesion } from '../redux/actions/auth';

const PrivateRoute = ({ children }) => {

  //const {state:{isAuth, email, consultando}, cerrarSesion,} = useContext(AuthContext);
  const dispatch = useDispatch();
  const {auth} = useSelector((state) => state);
  const {isAuth, consultando, email} = auth;

  console.log('ISAUTH', isAuth);

if(consultando){
  return "Cargando...";
}

  return (
    <>
      <section className='d-flex'>
        <Siderbard />
        <div style={{ width: '100%' }}>
          <Header email={email} cerrar={dispatch(cerrarSesion)} />
          <main className='container'>{isAuth ? children : <Navigate to='/'/>}</main>
        </div>
      </section>
    </>
  );
};

export default PrivateRoute;
