import React from 'react';
import {Navigate} from 'react-router-dom';
//import AuthContext from '../context/auth/AuthContext';
import { useSelector } from 'react-redux';

const PublicRoute = ({children}) => {

    const {auth} = useSelector((state) => state);
    const {isAuth, consultando} = auth;

    if(consultando){
        return "Cargando...";
    }
    return (
        <div>
            { isAuth ? <Navigate to='/dashboard' /> : children }
        </div>
    );
};

export default PublicRoute;