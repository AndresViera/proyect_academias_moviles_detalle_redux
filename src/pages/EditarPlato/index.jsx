import React, { useEffect, useContext } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import Formulario from '../../components/Formulario';
import ProductsContext from '../../context/products/ProductContext';
import { useDispatch, useSelector } from 'react-redux';
import { getByIdProducto, editarProducto } from '../../redux/actions/product';

const EditarPlato = () => {
  const params = useParams();
  const navigate = useNavigate();
  console.log(params);

  //const {getByIdProducto, producto} = useContext(ProductsContext);

  const dispatch = useDispatch();
  const { product } = useSelector((state) => state);
  const {editOK, producto} = product;

  useEffect(() => {
    //llamamos al obtener producto
    dispatch(getByIdProducto(params.id));
  }, []);

  useEffect(() => {
    if(editOK){
      navigate('./dashboard');
    }
  }, [editOK]);
  


  
  return (
    <div className='row'>
      <div className='col-12'>
        <h5 className='text-primary text-center py-3'>Editar Plato</h5>
        <div className='shadow-lg p-3 mb-5 bg-body rounded'>
        <Formulario producto={producto} editar={true} editarProducto={(producto) => dispatch(editarProducto(producto))} />
        </div>
      </div>
    </div>
  );
};

export default EditarPlato;
