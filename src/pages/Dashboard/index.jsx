import Table from '../../components/Table';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { obtenerProducto, eliminarProducto } from '../../redux/actions/product';

const Dashboard = () => {
  const dispatch = useDispatch();
  const { product } = useSelector((state) => state);
  const { listaProductos, deleteOK } = product;

  useEffect(() => {
    dispatch(obtenerProducto());
  },[]);

  useEffect(() => {
    if(deleteOK){
      dispatch(obtenerProducto());
    }
  },[deleteOK]);

  return (
    <>
    <h1>Productos</h1>
      <Table listaProductos={listaProductos} eliminarProducto={(id) => dispatch(eliminarProducto(id))}/>
    </>
  );
};

export default Dashboard;
