import Rutas from './routes';
import AuthState from './context/auth/AuthState';
import ProductState from './context/products/ProductState';
import {Provider} from 'react-redux';
import {store} from './redux/store';
function App() {
  return (
    <div className='App'>
     {/* <AuthState>
       <ProductState>
        <Rutas />
       </ProductState>
     </AuthState> */}
     <Provider store={store}>
      <Rutas/>
     </Provider>
    </div>
  );
}

export default App;
