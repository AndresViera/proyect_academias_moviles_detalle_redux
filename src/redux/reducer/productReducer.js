const initialState={
    listaProductos:[],
    addOk: false,
    deleteOK: false
}

const productReducer =(state = initialState, action) => {
    switch (action.type) {
        case 'AGREGAR_PRODUCTO':
            return {
                ...state,
                addOk: action.payload,
            };

            case 'LLENAR_PRODUCTOS':
                return {
                    ...state,
                    addOk: false,
                    deleteOK: false,
                    listaProductos: action.payload,
                };

            case 'ELIMINAR_PRODUCTO':
                return {
                    ...state,
                    deleteOK: action.payload,
                };    
            
        default:
            return state;
    
    }
    };
    
    export default productReducer;