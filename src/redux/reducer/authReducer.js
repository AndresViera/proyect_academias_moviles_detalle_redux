
 const initialState={
    email: null,
    isAuth: false,
    consultando: true
};

const authReducer =(state = initialState, action) => {
    switch (action.type) {
        case 'LOGIN':
            return {
                ...state,
                email: action.payload.email,
                isAuth: action.payload.isAuth
            };
        
            case 'UPDATE_IS_AUTH':
            return {
                ...state,
                email: action.payload.email,
                isAuth: action.payload.isAuth,
                consultando: action.payload.consultando
            };
    
            case 'LOGOUT':
            return {
                ...state,
                email: null,
                isAuth: false
            };
    
        default:
            return state;
    
    }
    };
    
    export default authReducer;