const reducer =(state, action) => {
    switch (action.type) {
        case 'AGREGAR_PRODUCTO':
            return {
                ...state,
                addOk: action.payload,
            };

            case 'LLENAR_PRODUCTOS':
                return {
                    ...state,
                    addOk: false,
                    deleteOK: false,
                    listaProductos: action.payload,
                };

            case 'ELIMINAR_PRODUCTO':
                return {
                    ...state,
                    deleteOK: action.payload,
                };
            
            case 'EDITAR_PRODUCTO':
                return {
                    ...state,
                    editOK: action.payload,
                };
            
            case 'OBTENER_PRODUCTO':
                return {
                    ...state,
                    producto: action.payload,
                };
            
        default:
            return state;
    
    }
    };
    
    export default reducer;