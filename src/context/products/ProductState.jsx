import React from 'react';
import { useReducer } from 'react';
import ProductContext from './ProductContext';
import ProductReducer from './ProductReducer';
import {db} from '../../firebase';

const ProductState = ({children}) => {
    const initialState={
        listaProductos:[],
        producto:{},
        addOk: false,
        deleteOK: false
    }

    const agregarProducto = async(producto)=>{
        console.log('el producto que llega al context', producto);
        try {
            await db.collection('productos').add(producto);
            dispatch({
                type: 'AGREGAR_PRODUCTO',
                payload: true,
            });
        } catch (error) {
            console.log(error);
        }
    }

    const obtenerProducto = async() =>{
        try {
            const productos = [];
            const info = await db.collection('productos').get();
            console.log({productos: info});
            info.forEach((item) =>{
                console.log(item.id);
                console.log(item.data());
                productos.push({
                    id: item.id,
                    ...item.data()
                });
            });
            console.log('Producto');
            console.log(productos);
            dispatch({
                type: 'LLENAR_PRODUCTOS',
                payload: productos,
            });
        } catch (error) {
            console.log(error);
        }
    }

    const eliminarProducto = async(id) =>{
        try {
            await db.collection('productos').doc(id).delete();
            dispatch({
                type: 'ELIMINAR_PRODUCTO',
                payload: true,
            })
        } catch (error) {
            console.log(error);
        }
    }

    const editarProducto = async (producto) =>{
        console.log('el producto a editar es:');
        console.log(producto);
        try {
            const productUpdate = {...producto};
            delete productUpdate.id;
            await db.collection('productos').doc(producto.id).update(productUpdate);
            dispatch({
                type: 'EDITAR_PRODUCTO',
                payload: true
            })
        } catch (error) {
            
        }
    }

    const getByIdProducto = async (id) =>{
        try {
            const info = await db.collection('productos').doc(id).get();
            // console.log(info.id);
            // console.log(info.data());
            let producto = {
                id: info.id,
                ...info.data(),
            };
            dispatch({
                type: 'OBTENER_PRODUCTO',
                payload: producto
            });
        } catch (error) {
            console.log(error);
        }
    }

    const [state, dispatch] = useReducer(ProductReducer, initialState)
    return (
        <ProductContext.Provider value={{ 
            listaProductos: state.listaProductos,
            addOk: state.addOk, 
            deleteOK: state.deleteOK, 
            producto: state.producto, 
            agregarProducto, 
            eliminarProducto, 
            obtenerProducto, 
            getByIdProducto,  
            editarProducto
            }}>
            {children}
        </ProductContext.Provider>
    );
};

export default ProductState;